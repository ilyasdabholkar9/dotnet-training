﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7Task.Exceptions
{
    internal class ContactAlreadyExistsException : Exception
    {
        public ContactAlreadyExistsException()
        {

        }

        public ContactAlreadyExistsException(string name) : base()
        {
            Console.WriteLine($"Contact With Name {name} already exists");
        }
    }
}
