﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7Task.Model
{
    internal class Contact
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }

        public string GetContactDetails()
        {
            return $"Name : {Name} Address : {Address} City : {City} PhoneNumber: {PhoneNumber}";
        }
    }
}
