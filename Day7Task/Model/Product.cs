﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7Task.Model
{
    internal class Product
    {
        public int id;
        public string name;
        public Product(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
