﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7Task.Model
{
    internal class ShoppingProduct
    {
        public int id;
        public int quantity;

        public ShoppingProduct(int id, int quantity)
        {
            this.id = id;
            this.quantity = quantity;
        }
    }
}
