﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7Task.Model
{
    internal class User
    {
        public int id;
        public string name;

        public User(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public object this [int index]
        {
            get
            {
                if (index == 0)
                    return id;
                else if (index == 1)
                    return name;
                else
                    return null;
            }
            set
            {
                if (index == 0)
                    id = Convert.ToInt32(value);
                else if (index == 1)
                    name = value.ToString();
            }
        }
    }
}
    