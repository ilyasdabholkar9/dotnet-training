﻿// See https://aka.ms/new-console-template for more information

using Day7Task.Exceptions;
using Day7Task.Model;
using Day7Task.Repository;

int input = 0;
ContactRepository contactRepo = new ContactRepository();

while (input != 6)
{
    Console.WriteLine("--------------------------------");
    Console.WriteLine("\nSelect Operation : ");
    Console.WriteLine("1) Add Contact");
    Console.WriteLine("2) Update Contact");
    Console.WriteLine("3) Delete Contact");
    Console.WriteLine("4) Get All Contacts");
    Console.WriteLine("5) Get Contacts By City");
    Console.WriteLine("6) Exit");
    Console.WriteLine("--------------------------------");
    input = int.Parse(Console.ReadLine());


    switch (input)
    {
        case 1:
            try
            {
                Contact newContact = new Contact();
                Console.WriteLine("Enter Contact Name : ");
                newContact.Name = Console.ReadLine();

                Console.WriteLine("Enter Contact Address : ");
                newContact.Address = Console.ReadLine();

                Console.WriteLine("Enter Contact City : ");
                newContact.City = Console.ReadLine();

                Console.WriteLine("Enter Contact PhoneNumber : ");
                newContact.PhoneNumber = Console.ReadLine();

                Console.WriteLine(contactRepo.AddContact(newContact));
            }
            catch (ContactAlreadyExistsException ex)
            {
                Console.WriteLine(ex.Message);
            }
            break;
        case 2:

            Console.WriteLine("Enter Name Of Contact To Be Updated : ");
            string cName = Console.ReadLine();

            Contact updateContact = new Contact();
            updateContact.Name = cName;

            Console.WriteLine("Enter Contact Address : ");
            updateContact.Address = Console.ReadLine();

            Console.WriteLine("Enter Contact City : ");
            updateContact.City = Console.ReadLine();

            Console.WriteLine("Enter Contact PhoneNumber : ");
            updateContact.PhoneNumber = Console.ReadLine();

            contactRepo.UpdateContact(cName, updateContact);


            break;
        case 3:
            Console.WriteLine("Enter Name Of Contact To Be Deleted : ");
            string cDelete = Console.ReadLine();
            Console.WriteLine(contactRepo.DeleteContact(cDelete));
            break;
        case 4:
            List<Contact> contacts = contactRepo.GetAllContacts();
            foreach (Contact contact in contacts)
            {
                Console.WriteLine(contact.GetContactDetails());
            }
            break;
        case 5:
            Console.WriteLine("Enter Name Of City : ");
            string city = Console.ReadLine();
            contactRepo.GetContactByCity(city);
            break;
        case 6:
            input = 6;
            break;
        default:
            Console.WriteLine("Not A Valid Choice");
            break;
    }
}

// Join
Product p1 = new Product(1,"TV");
Product p2 = new Product(2,"Moile");

ShoppingProduct sp1 = new ShoppingProduct(1,4);

ShoppingProduct sp2 = new ShoppingProduct(2,2);

List<Product> productList = new List<Product> { p1,p2 };
List<ShoppingProduct> shopProductList = new List<ShoppingProduct> { sp1,sp2 };

var result = from p in productList
             join sp in shopProductList
             on p.id equals sp.id
             select new
             {
                 productName = p.name,
                 productQty = sp.quantity
             };

foreach(var item in result)
{
    Console.WriteLine($"Name : {item.productName} Quantity : {item.productQty}");
}

//Indexers
User user1 = new User(1, "Ilyas");

Console.WriteLine($"Id : {user1[0]} Name: {user1[1]}");

user1[0] = 3;
user1[1] = "Josef";
Console.WriteLine($"Id : {user1[0]} Name: {user1[1]}");