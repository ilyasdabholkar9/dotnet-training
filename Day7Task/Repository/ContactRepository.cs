﻿using Day7Task.Exceptions;
using Day7Task.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7Task.Repository
{
    internal class ContactRepository
    {
        public List<Contact> contacts;

        public ContactRepository()
        {
            contacts = new List<Contact>() {
                new Contact() { Name="josef",Address="pune",City="pune",PhoneNumber="123456789"},
                new Contact() { Name="ilyas",Address="panvel",City="panvel",PhoneNumber="123456789"}
            };
        }

        public string AddContact(Contact c)
        {
            Contact contact = contacts.FirstOrDefault(el => el.Name == c.Name);

            if (contact == null)
            {
                contacts.Add(c);
                return "Contact Added";
            }
            else
            {
                throw new ContactAlreadyExistsException(c.Name);
            }
        }


        public string UpdateContact(string name, Contact c)
        {
            Contact contact = contacts.FirstOrDefault(el => el.Name == name);
            contact.Address = c.Address;
            contact.PhoneNumber = c.PhoneNumber;
            contact.City = c.City;
            return "contact updated";
        }

        public string DeleteContact(string name)
        {
            Contact contact = contacts.FirstOrDefault(el => el.Name == name);
            contacts.Remove(contact);
            return "Contact deleted";
        }

        public List<Contact> GetAllContacts()
        {
            return contacts;
        }

        public void GetContactByCity(string city)
        {
            var filtered = contacts.Where(el => el.City == city);
            foreach(var contact in filtered)
            {
                Console.WriteLine(contact.GetContactDetails());
            }
        }
    }
}
