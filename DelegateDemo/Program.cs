﻿// See https://aka.ms/new-console-template for more information

public class Program
{
    public static void sayHello(string name)
    {
        Console.WriteLine($"Hello {name}");
    }

    public static void Main()
    {
        Predicate<string> test = delegate (string name)
        {
            if (name.Length > 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        Console.WriteLine(test("ilyasdabholkar"));
        
        Action<string> ActionDelegate = sayHello;
        ActionDelegate("ilyas");

        Func<int, int> SquareNum = delegate (int num)
        {
            return num * num;
        };

        Console.WriteLine(SquareNum());


    }



    
}