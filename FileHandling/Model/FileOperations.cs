﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Model
{
    internal class FileOperations
    {
        public string File { get; set; }
        public bool writeContent(string content)
        {
            using (StreamWriter writer = new StreamWriter(File,true))
            {
                writer.WriteLine(content);
            }
            return true;
        }

        public bool verifyContent(string content)
        {
            using (StreamReader reader = new StreamReader(File))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] arr = line.Split(',');
                    foreach(string item in arr)
                    {
                        if(item == content)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
                
        }

        public void showFileContents()
        {
            using (StreamReader reader = new StreamReader(File))
            {
                //string content = reader.ReadToEnd();
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }
    }
}
