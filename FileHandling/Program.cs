﻿// See https://aka.ms/new-console-template for more information

using FileHandling.Exceptions;
using FileHandling.Model;
using FileHandling.Repository;

int input = 0;

while (input != 4)
{
    Console.WriteLine("--------------------------------");
    Console.WriteLine("\nSelect Operation : ");
    Console.WriteLine("1) Register User");
    Console.WriteLine("2) Check For A User");
    Console.WriteLine("3) View All File Contents");
    Console.WriteLine("4) Exit");
    Console.WriteLine("--------------------------------");
    input = int.Parse(Console.ReadLine());

    UserRepo repo = new UserRepo();

    switch (input)
    {
        case 1:
            User user = new User();
            Console.WriteLine("Enter User Id : ");
            user.Id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter User Name : ");
            user.Name = Console.ReadLine();

            Console.WriteLine("Enter User City : ");
            user.City = Console.ReadLine();
            repo.registerUser(user);
            break;
        case 2:
            try
            {
                Console.WriteLine("Enter Name Of The User : ");
                string userToCheck = Console.ReadLine();
                Console.WriteLine(repo.verifyUser(userToCheck));
            }
            catch(UserNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            break;
        case 3:
            repo.showDetails();
            break;
        default:
            Console.WriteLine("Not A Valid Choice");
            break;
    }
}





