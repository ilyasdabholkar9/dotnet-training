﻿using FileHandling.Exceptions;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepo
    {
        FileOperations FileObj;
        public UserRepo()
        {
            FileObj = new FileOperations();
            FileObj.File = "users.txt";
        }
        public bool registerUser(User user)
        {

            FileObj.writeContent(user.getUserData());
            return true;
        }

        public string verifyUser(string name)
        {
            if (FileObj.verifyContent(name))
            {
                return "UserExists";
            }
            else
            {
                throw new UserNotFoundException(name);
            }
        }

        public void showDetails()
        {

            FileObj.showFileContents();
        }


    }
}
