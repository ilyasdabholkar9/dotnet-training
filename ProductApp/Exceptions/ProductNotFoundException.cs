﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Exceptions
{
    internal class ProductNotFoundException : Exception
    {
        public ProductNotFoundException()
        {

        }

        public ProductNotFoundException(int id)
        :base($"The Product With Id {id} Does Not Exist")
        {

        }
    }
}
