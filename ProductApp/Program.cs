﻿// See https://aka.ms/new-console-template for more information

using ProductApp.Exceptions;
using ProductApp.Model;
using ProductApp.Repository;

ProductRepo repo = new ProductRepo();
Product p1 = new Product() { Id = 1, Name = "Shirt", Price = 499.5f };
Product p2 = new Product() { Id = 2, Name = "Pant", Price = 429.52f };
Product p3 = new Product() { Id = 3, Name = "Watch", Price = 799.55f };
Product p4 = new Product() { Id = 4, Name = "Shoes", Price = 799.55f };

Console.WriteLine(repo.addProduct(p1));
Console.WriteLine(repo.addProduct(p2));
Console.WriteLine(repo.addProduct(p3));

repo.showProducts();

Console.WriteLine(repo.updateProduct(3,"Belt",500.2f));
repo.showProducts();

Console.WriteLine(repo.getProductByName("Belt"));

Product[] afterDelete = repo.deleteProduct(p3);
foreach(Product p in afterDelete)
{
    Console.WriteLine($"Name : {p.Name}");
}

//Exception here
try
{
    Product[] excDelete = repo.deleteProduct(p4);
}
catch(ProductNotFoundException ex)
{
    Console.WriteLine(ex.Message);
}
