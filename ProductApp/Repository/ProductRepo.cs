﻿using ProductApp.Exceptions;
using ProductApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Repository
{
    
    internal class ProductRepo
    {
        Product[] products = new Product[3];

        int count = 0;
        public ProductRepo()
        {

        }

        public string addProduct(Product p)
        {
            products[count] = p;
            count++;
            return "Product added";
        }

        public Product[] deleteProduct(Product p)
        {
            Product temp = Array.Find(products, el => el.Id == p.Id);
            if(temp != null)
            {
                return Array.FindAll(products, el => el.Id != p.Id);
            }
            else
            {
                throw new ProductNotFoundException(p.Id);
            }
        }

        public string updateProduct(int id,string name,float price)
        {
            Product temp = Array.Find(products, el => el.Id == id);
            temp.Name = name;
            temp.Price = price;
            return "Product Updated";
        }

        public string getProductByName(string name)
        {
            Product p = Array.Find(products, el => el.Name == name);
            return $"Id : {p.Id} Name : {p.Name} Price : {p.Price}";
        }

        public void showProducts()
        {
            foreach(Product p in products)
            {
                Console.WriteLine($"Id : {p.Id} Name : {p.Name} Price : {p.Price}");
            }
        }
    }
}
