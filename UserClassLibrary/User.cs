﻿namespace UserClassLibrary
{
    public class User
    {
        public int id=11;
        public string firstName;
        public string lastName;
        public string email;

        public User()
        {

        }

        public User(string firstName, string lastName, string email)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
        }

        public string getUserDetails()
        {
            string details = $"Id : {id} \nName : {firstName} {lastName} \nEmail : {email}";
            return details;
        }

        public void setUserDetails(string firstName,string lastName, string email)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
        }
    }
}